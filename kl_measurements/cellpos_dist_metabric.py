import numpy as np
import pandas as pd
import os
import math
from sklearn.neighbors import KernelDensity
from scipy.stats import entropy
from sklearn.metrics import mutual_info_score
import pickle
from multiprocessing import Pool
import functools


### The path to the csv files
cellpos_path = '/data/shan/cellPos/'
### Define 4 classes by letters
class_tumor = 'c'
class_lymphocyte = 'l'
class_fibroblast = 'o'
class_other = 'a'
#classes = {class_tumor:4, class_lymphocyte:3, class_fibroblast:2, class_other:1}
### 50 microns was the maximum effective range of a lymphocyte cell 
cell_effect_radius = 50 # in microns
### Just for numerical stability
minval = 1e-100


### To get a conversion ratio from microns to pixels,
### please fill in the below according to one of the files, preferably one with more cells
example_slide_csv = '1667_HE.csv'
y_microns = 7425 # in microns
x_microns = 10220 # in microns


### Calculating the conversion ratio
s = pd.read_csv(cellpos_path + example_slide_csv)
x_pixels = s.max().x - s.min().x
y_pixels = s.max().y - s.min().y
m2p_ratio = (x_pixels/x_microns + y_pixels/y_microns) / 2
micron2pixels = lambda x: x * m2p_ratio
kde_kernel_bandwidth = micron2pixels(cell_effect_radius) 
print(x_pixels, y_pixels, x_microns, y_microns, m2p_ratio, kde_kernel_bandwidth)


### the smaller you set the below number, the better the estimate, also take much longer
### 5 pixels was a good place that it did not took too long and the shape of the dists looked good
grid_size_pixles = 5


### Number of paraller processes, I had a 24 core cpu
num_process = 22
output_binary_file = 'cellpos_dist_KL.pkl'


### the calcluation function for KL and dists 
min_white_area_pixels = kde_kernel_bandwidth


def cartesian(x1, x2):
    return np.transpose([np.tile(x1, len(x2)), np.repeat(x2, len(x1))])


def get_stats(file_name):
    print(file_name)
    try:
        s = pd.read_csv(cellpos_path + file_name)
        gx = np.array(range(0, s['x'].max() + s['x'].min(), grid_size_pixles)) #/ cell_effect_radius
        gy = np.array(range(0, s['y'].max() + s['y'].min(), grid_size_pixles)) #/ cell_effect_radius
        grid = cartesian(gx, gy)
        cl = s[['x', 'y']][s['class'] == class_lymphocyte].values #/ cell_effect_radius # divide just for fun
        ct = s[['x', 'y']][s['class'] == class_tumor].values #/ cell_effect_radius
        cf = s[['x', 'y']][s['class'] == class_fibroblast].values #/ cell_effect_radius
        cells = pd.DataFrame(np.round(s[['x', 'y']].values / min_white_area_pixels).astype(int), columns=['_x', '_y']).drop_duplicates().copy()
        cells['d'] = 1
        gdf = pd.DataFrame(grid, columns=['x', 'y'])
        gdf['_x'] = np.round(gdf['x'].values / min_white_area_pixels).astype(int)
        gdf['_y'] = np.round(gdf['y'].values / min_white_area_pixels).astype(int)
        smart_grid = gdf.merge(cells, left_on=['_x', '_y'], right_on=['_x', '_y'], how='inner')[['x', 'y']].values#.dropna(subset=['d'])
        kt = KernelDensity(kernel='gaussian', bandwidth=kde_kernel_bandwidth, algorithm='ball_tree', leaf_size=20).fit(ct)
        kl = KernelDensity(kernel='gaussian', bandwidth=kde_kernel_bandwidth, algorithm='ball_tree', leaf_size=20).fit(cl)
        kf = KernelDensity(kernel='gaussian', bandwidth=kde_kernel_bandwidth, algorithm='ball_tree', leaf_size=20).fit(cf)
        dt = np.exp(kt.score_samples(smart_grid))
        dl = np.exp(kl.score_samples(smart_grid))
        df = np.exp(kf.score_samples(smart_grid))
        dt[dt < minval] = minval
        dl[dl < minval] = minval
        df[df < minval] = minval
        dt = dt / sum(dt)
        dl = dl / sum(dl)
        df = df / sum(df)
        return {file_name: 
                {'kl_tl': entropy(dt, dl), 'kl_tf': entropy(dt, df), 'kl_fl': entropy(df, dl), 
                 'count_t': ct.shape[0], 'count_l': cl.shape[0], 'count_f': cf.shape[0]}}
    except:
        return {file_name: None}


### the main run
if __name__ == "__main__":
    onlyfiles = [str(f) for f in os.listdir(cellpos_path) if os.path.isfile(cellpos_path + f)]
    pool = Pool(num_process)
    poolres = pool.map(get_stats, onlyfiles)
    
    p_measures = {}
    for d in poolres:
        p_measures.update(d)
        
    print(p_measures)
    with open(output_binary_file, 'wb') as f:
        pickle.dump(p_measures, f)
