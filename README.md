# Submission_Nederlof_Hajizadeh_2021
This repository includes all the Python and R scripts used for the results presented in our submission tiltled "Spatial  analysis  of  lymphocytes  and  fibroblasts identifiesbiological relevant patterns inestrogen positivebreast cancer".

The running sequence is as follows:

1- SSCNN with inception-v3 core trianing, and scoring

2- Filtering out the detectios that fall putside the per slide manually annotated tumor area for the ICGC cohort in a posprocessing step

3- Collecting detections for the METABRIC cohort from a previous study

4- Fitting empirical densities using kernel density estimators for each of the studied 3 cell types and calculating cell ratios and KL measures

5- Clustring analysis using hierarchical clustering for both cohorts

6- UMAP analysis to check clustring robustness between the two cohorts

7- Testing Chi2 contingency of the clustering against several clinical variables for both cohorst

8- Fitting uni- and multivariate cox proportinal hazard regression for METABRIC cohort with survival outcome data

9- Plotting the Kaplan-Meier curves for significant clusters vs the rest


## Dependencies

Software:

- R version 4.0
- Python 3.8

packages:

- matplotlib v3.1.1
- lifelines v0.25
- scipy v1.5.4
- umap v0.1.1
- seaborn v0.11.1
- numpy v1.16.4
- pandas v1.1.5
- sklearn v0.24.1

